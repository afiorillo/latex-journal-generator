#!/usr/bin/env bash

# this script is meant to be used like:
#
# $ ./scripts/build.sh journal.tex
# $ ./scripts/build.sh <filename.tex inside of the tex/ directory> 

set -x

REPO_DIR="$(git rev-parse --show-toplevel)"

docker run \
    --rm \
    -it \
    -v "$REPO_DIR:/data" \
    -u $(id -u):$(id -g) \
    moss/xelatex \
    xelatex --halt-on-error --output-directory=/data/out "tex/$1"
