#!/usr/bin/env python

# this script should take some arguments, such as:
# 1. how many days forward to generate
# 2. what fields per day
# 3. where to write the file to
#
# and generate an according tex file from a template

from dataclasses import dataclass, asdict
from datetime import datetime, timedelta
from pathlib import Path

import jinja2
import click

REPO_ROOT = Path(__file__).parent.parent

TEMPLATES = {
      "wrapper": r"""
\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[english]{babel}

% Set page size and margins
% Replace `letterpaper' with`a4paper' for UK/EU standard size
\usepackage[a4paper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{array}
\usepackage{tabularx}
\usepackage{datetime}
\usepackage{fancyhdr}
\usepackage{float}
\pagestyle{fancy}
\pagenumbering{gobble}
\fancyfoot[L]{Generated on \today \ at \currenttime}


\begin{document}

{{ day_tables }}

\end{document}
""",
      "day_table": r"""
\subsection*{ {{ pretty_date}} }

\begin{table}[H]
\begin{tabularx}{\textwidth}{l > {\raggedleft\arraybackslash}X}
      {{ field_lines }}
      \hline
\end{tabularx}
\end{table}
""",
    "field_line": r"""
      \hline
      {{ prompt }} & \\ \noalign{\vspace{ {{ size }} }}
"""
}

@dataclass
class JournalField:
      prompt: str # ex "Food log"
      size: str  # ex "2em"

DEFAULT_FIELDS = [
      JournalField('Food log', '2em'),
      JournalField('Today in 50 words', '4em'),
]

@click.command('generate.py')
@click.option('-o', '--output')
@click.option('-n', '--num-days', type=int, default=5)
def main(output: str, num_days: int):
      out_fp = REPO_ROOT.joinpath('tex', output)

      loader = jinja2.DictLoader(TEMPLATES)
      env = jinja2.Environment(loader=loader)

      field_lines = []

      for journal_field in DEFAULT_FIELDS:
            field_lines.append(env.get_template('field_line').render(asdict(journal_field)))

      ctx = {'field_lines': '\n'.join(field_lines)}

      day_tables = []
      for days_from_today in range(num_days):
            day = datetime.today() + timedelta(days=days_from_today)
            ctx['date'] = day
            ctx['pretty_date'] = day.strftime('%Y-%m-%d, %A')
            tpl = env.get_template('day_table')
            day_tables.append(tpl.render(ctx))
      
      ctx['day_tables'] = '\n'.join(day_tables)
      wrapped = env.get_template('wrapper').render(ctx)
      out_fp.write_text(wrapped)


if __name__ == '__main__':
      main()
