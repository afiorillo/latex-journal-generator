
.PHONY: clean scrub next_week

clean:
	rm -f out/*.{ps,log,aux,out,dvi,bbl,blg,snm,toc,nav}

scrub: clean
	rm -f out/*.pdf

one_page:
	./scripts/generate.py -o onepage.tex -n 5
	./scripts/build.sh onepage.tex

next_week:
	./scripts/generate.py -o next_week.tex -n 7
	./scripts/build.sh next_week.tex
