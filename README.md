# A generative journal

Paper journals are nice.
It is humbling to remember that for the last few thousand years, a person would *write* their thoughts rather than type them.

This repository contains scripts for generating a daily journal as a printable PDF.
The idealized workflow is along the lines of:

1. Update the `DEFAULT_FIELDS` in `generate.py` with your desired fields.
2. Run a make recipe, such as `make next_week` and an `out/next_week.pdf` file will be created.
3. Print that PDF file and put it in a nice binder, then stop using a computer for a while.

See an example [here](out/example.pdf).

## Setup

The Python script uses `poetry` for package management, so a `poetry install` should install those dependencies.

The typesetting is done with `xelatex` via a Docker container (`moss/xelatex`).

## Todo

- [ ] Add some daily context to the subsection heading, e.g. the weather forecast, lunar phase, sunrise/sunset
- [ ] Add non-daily/random fields. E.g. every 5 days or 4% chance to add "Deep cleaned the kitchen?"
- [ ] 2 column formatting, or something more dense at least
- [ ] Typed fields: checkboxes, long text, short text

## Disclaimer

Assume this project is unmaintained.
Use with caution!